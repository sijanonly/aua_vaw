from django.conf.urls import patterns, include, url
from iaua_app.views import *
from iaua_app.view1 import *
from iaua_app.view2 import *
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
	url(r'^$', 'iaua_app.views.load_ngos', name='home'),



    url(r'^$','iaua_app.views.default_page',name = 'home'),
	url(r'^loadngo$', 'iaua_app.views.load_ngos', name='home'),
    #url(r'^load/incidents', 'iaua_app.view1.load_incidents', name='home'),
    #url(r'^$','iaua_app.views.default_page',name = 'home'),
	#url(r'^$', 'iaua_app.views.load_ngos', name='home'),

	#url(r'^load/', 'iaua_app.view1.load_programs', name='home'),
    url(r'^ngolist$','iaua_app.view2.number_of_org'),



	url(r'^load$', 'iaua_app.view1.load_programs', name='home'),

	url(r'^load1','iaua_app.view2.load_district'),
    url(r'^index/data.js','iaua_app.view2.number_of_ingos'),
    url(r'^programme/(\d+?)/?$','iaua_app.view2.programme'),
    url(r'^program/(\d+?)/?$','iaua_app.view2.programme'),
    url(r'^ingo/(\d+?)/?$','iaua_app.view2.ingo'),
    # Examples:
    # url(r'^$', 'iaua.views.home', name='home'),
    # url(r'^iaua/', include('iaua.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),

)

