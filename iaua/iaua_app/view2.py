from iaua_app.models import *
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
import json
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt

def number_of_org(request):

	if request.POST['district'] != None:

		
		ingo_info = []
		try:
			district = District.manager.get(district_name=request.POST['district'].capitalize())

			districtingo = I_ngo.manager.filter(address = district)
			districtingo = I_ngo.manager.filter(address = request.POST['district'].capitalize())
			for ingo in districtingo:
				ingo_info.append({'name':ingo.name,'contact_person':ingo.contact_person,'id':ingo.id,'contact_number':ingo.contact_number,'org_type':ingo.org_type})
				pass
		except:
			for ingo in districtingo:
				ingo_info.append({'name':ingo.name,'contact_person':ingo.contact_person,'id':eachIngo.id,
					'contact_number':ingo.contact_number,'org_type':ingo.org_type})

		except:
			pass

		#programme = Programme.manager.filter(ingo = districtingo)
		return HttpResponse(json.dumps({'result':ingo_info}))

def number_of_ingos(request):
	
	parameter = {}
	
	district = District.manager.all()
	number_of_ingo = 'var ingodata = {'
	for eachDistrict in district:
		try:
			ingo = I_ngo.manager.filter(address = eachDistrict)
			number_of_ingo+="'"+str(eachDistrict.district_name)+"':"+str(len(ingo))+","
			
		except:
			pass

	number_of_ingo += '};\n'

	number_of_programme = 'var programmedata = {'
	for eachDistrict in district:
		try:
			programme = Programme.manager.filter(district = eachDistrict)
			budget = 0.0
			for eachP in programme:

				budget += eachP.budget
			number_of_programme+="'"+str(eachDistrict.district_name)+"':"+budget+","
			
		except:
			pass

	number_of_programme += '};\n'

	number_of_incidents = 'var incidentdata = {'
	for eachDistrict in district:
		try:
			incident = Incident.manager.filter(district = eachDistrict)
			number_of_incidents+="'"+str(eachDistrict.district_name)+"':"+str(len(incident))+","
			
		except:
			pass

	number_of_incidents += '};\n'

	total_data = number_of_ingo + number_of_programme + number_of_incidents

	return HttpResponse((total_data))




	# ingo_big = []
	
	# for eachDistrict in district:
		
	# 	try:
	# 		ingo = I_ngo.manager.filter(address = eachDistrict)
		
	# 		ingo_big.append({'number':len(ingo),'district_name':eachDistrict.district_name})
	# 	except:
	# 		ingo = None


	# return HttpResponse(json.dumps({'result':ingo_big}))
			
	
	# ingo_info = []
	# try:

	# 	districtingo = I_ngo.manager.filter(address = request.POST['district'].capitalize())
	# 	for eachIngo in districtingo:
	# 		ingo_info.append({'name':ingo.name,'contact_person':ingo.contact_person,'id':eachIngo.id
	# 			'contact_number':ingo.contact_number,'org_type':ingo.org_type})

	# except:
	# 	pass

	# #programme = Programme.manager.filter(ingo = districtingo)
	# return HttpResponse(json.dumps({'result':ingo_info}))



def ingo(request,id):
	parameter = {}
	try:
		ingo = I_ngo.manager.get(id=id)
	except:
		ingo = None

	print ingo.name, ingo.address.district_name, ingo.contact_person, ingo.contact_number

	parameter['ingo'] = ingo


	program_list = Programme.manager.filter(ingo = ingo)
	parameter['program'] = program_list

	return render_to_response('ingo.html',parameter)

def programme(request,id):
	parameter =  {}

	try:
		programme = Programme.manager.get(id = id)
	except:
		programme = None

	parameter['program'] = programme

	return render_to_response('programme.html',parameter)

def incident(request):
	pass



def load_district(request):
	district = ['Taplejung','Panchthar','Ilam','Jhapa','Morang','Sunsari','Dhankuta','Terhathum',
	'Sankhuwasabha','Bhojpur','Solukhumbu','Okhaldhunga','Khotang','Udayapur','Saptari','Siraha',
	'Dhanusha','Mahottari','Sarlahi','Sindhuli','Ramechhap','Dolakha','Sindhupalchok','Kavrepalanchok',
	'Lalitpur','Bhaktapur','Kathmandu','Nuwakot','Rasuwa','Dhading','Makwanpur','Rautahat','Bara','Parsa',
	'Chitwan','Gorkha','Lamjung','Tanahu','Syangja','Kaski','Manang','Mustang','Myagdi','Parbat','Baglung',
	'Gulmi','Palpa','Nawalparasi','Rupandehi','Kapilbastu','Arghakhanchi','Pyuthan','Rolpa','Rukum','Salyan',
	'Dang','Banke','Bardiya','Surkhet','Dailekh','Jajarkot','Dolpa','Jumla','Kalikot','Mugu',
	'Humla','Bajura','Bajhang','Achham','Doti','Kailali','Kanchanpur','Dadeldhura','Baitadi','Darchula'];


	file = open("datafile.txt", "wb")
	i = 1
	for eachDistrict in district:
		content = None
		content = 	"\n\n- model: iaua_app.district\n"
 		content +=	"\tpk: " + str(i) 
  		content	+=	"\n\tfields:\n"
   		content +=  "\t\tdistrict_name: "+eachDistrict
   		file.write(content)
   		i = i+1
	file.close()


	# for eachDistrict in district:
	# 	district = District(district_name = eachDistrict)
	# 	district.save()
