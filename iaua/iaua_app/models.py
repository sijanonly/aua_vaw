from django.db import models
from iaua_app.manager import *

from model_utils import Choices

# Create your models here.

class District(models.Model):
	district_name = models.CharField(max_length = 50)
	manager = DistrictManager()

class I_ngo(models.Model):
	name = models.CharField(max_length = 150)
	address = models.ForeignKey(District)
	contact_person = models.CharField(max_length = 50)
	contact_number = models.CharField(max_length = 50)
	org_type = models.CharField(max_length = 60)
	manager = IngoManager()

# class Budget(models.Model):
# 	ingo = models.ForeignKey(I_ngo)
# 	year = models.CharField(max_length = 10)
# 	budget = models.IntegerField(max_length = 10000000000 )

class Programme(models.Model):
	ingo = models.ForeignKey(I_ngo)
	program_name = models.CharField(max_length=500)
	description = models.CharField(max_length = 2000)
	year = models.CharField(max_length = 10)
	budget = models.FloatField()
	donors = models.CharField(max_length = 200)
	district = models.ForeignKey(District)
	contact_person = models.CharField(max_length = 100)
	manager = ProgrammeManager()

class Incident(models.Model):
	# STATUS = Choices('rape', 'sex')

	district = models.ForeignKey(District)
	title = models.CharField(max_length=300)
	incident_type = models.CharField(max_length=100)
	victim_number = models.CharField(max_length=20)
	year = models.CharField(max_length = 10)
	mananger = IncidentManager()