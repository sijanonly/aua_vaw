from iaua_app.models import *
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
import json
from django.views.decorators.csrf import csrf_exempt
import random
from xlrd import *
import string
import datetime
import csv

#constants
INGO_NAME=1
PROGRAM_NAME=2
def load_incidents(request):
	fi = open("GenderBasedViolence_WHRD.csv","rb")
	name = csv.reader(fi)
	dataDict = []
	i =0
	for eachRow in name:
		title = eachRow[1].split('|')[0]
		mydics=eachRow[3].split(',')[-1]
		mydics=mydics.strip()
		print mydics
		if ('woman'or 'rape' or 'witch' or 'rapist' or 'girl' or 'girls' or 'women' or 'raping' or 'wife' or 'dowry' or 'raped' or 'polygamy' or 'teen' or 'traffickinng') in title.lower():
			i +=1
			params = {
				'title':title, 
				'date':eachRow[2], 
				'district':mydics, 
				'catetory' : eachRow[5],
				'numberOfDeath':eachRow[9], 
				'numberOfInjuries':eachRow[10]
				}
			print params.district
			mydistrict = District.manager.get(district_name = params.district)
			victim_no=int(params.numberOfInjuries)+int(params.numberOfDeath)
			incident=Incident(district= mydistrict, title= params.title,incident_type= params.catetory, victim_number=victim_no,year= params.date)
			incident.save()

def load_programs(request):
	programs = open_workbook('NGO_Programs.xlsx')
	district_l = ['Taplejung','Panchthar','Ilam','Jhapa','Morang','Sunsari','Dhankuta','Terhathum',
	'Sankhuwasabha','Bhojpur','Solukhumbu','Okhaldhunga','Khotang','Udayapur','Saptari','Siraha',
	'Dhanusha','Mahottari','Sarlahi','Sindhuli','Ramechhap','Dolakha','Sindhupalchok','Kavrepalanchok',
	'Lalitpur','Bhaktapur','Kathmandu','Nuwakot','Rasuwa','Dhading','Makwanpur','Rautahat','Bara','Parsa',
	'Chitwan','Gorkha','Lamjung','Tanahu','Syangja','Kaski','Manang','Mustang','Myagdi','Parbat','Baglung',
	'Gulmi','Palpa','Nawalparasi','Rupandehi','Kapilbastu','Arghakhanchi','Pyuthan','Rolpa','Rukum','Salyan',
	'Dang','Banke','Bardiya','Surkhet','Dailekh','Jajarkot','Dolpa','Jumla','Kalikot','Mugu',
	'Humla','Bajura','Bajhang','Achham','Doti','Kailali','Kanchanpur','Dadeldhura','Baitadi','Darchula'];
	
	for i in range(103):
		sheet = programs.sheet_by_index(i)
		print sheet.name
		print i
		if i==0 or i==102:
			DISTRICTS=4
			DONOR=5
			BUDGET=9
			DURATION=11
			SECTOR=12
			CONTACT_PERSON=13
		else:
			DISTRICTS=3
			DONOR=4
			BUDGET=8
			DURATION=9
			SECTOR=10
			CONTACT_PERSON=11

		for row in range(sheet.nrows):
			
			if i==0 and row<=5:
				continue
		 	else:
		 		if row ==0:
		 			continue
		 	Sector = str(sheet.cell_value(row, SECTOR))
		 	Sector=Sector.lower()
		 	Sector = Sector.strip()
		 	#print Sector
		 	#filter out NGOs only working in women sector
		 	if Sector.find('women')==-1:
		 		continue
		 	#splitting name n address and selecting only name of INGO
		 	Ingo_name = sheet.cell_value(row, INGO_NAME)
		 	name_list=Ingo_name.split(",")
		 	addr="N/A"
		 	if len(name_list)>1:
		 		addr=name_list[-1]
		 		addr=addr.replace(".", "")
		 	name_list = name_list[:len(name_list)-1]
		 	Ingo_name=" ".join(name_list)
		 	#print Ingo_name

		 	Org_type = "NGO"
		 	Contact_no ="N/A"
		 	Contact_person = sheet.cell_value(row, CONTACT_PERSON)
		 	contact_list = Contact_person.split(",")
		 	if len(contact_list)>1:
		 		Contact_no= contact_list[-1]
		 		Contact_person = contact_list[:len(contact_list)-1]
		 		Contact_person=" ".join(Contact_person)
		 	
		 	Program_name = sheet.cell_value(row, PROGRAM_NAME)	
		 	Description = "N/A"
		 	Donor = sheet.cell_value(row, DONOR)
		 	Budget = str(sheet.cell_value(row, BUDGET))

		 	Budget=Budget.replace(",","")
		 	Budget=float(Budget)
		 	Duration = sheet.cell_value(row, DURATION)
		 	try:
		 		obj_ingo = I_ngo.manager.get(name= Ingo_name.strip())
		 		Districts = sheet.cell_value(row, DISTRICTS)	
			 	dist_list=[]
			 	for eachdistricts in district_l:
			 		if Districts.find(eachdistricts)!=-1:
			 			dist_list.append(eachdistricts)
			 	print Ingo_name
			 	print Program_name
			 	print dist_list
			 	print Donor
			 	print Budget
			 	print Duration
			 	print Sector
			 	print Contact_person
			 	for eachdist in dist_list:
			 		try:
			 			print "trying to save programme1"
			 			mydistrict = District.manager.get(district_name = eachdist)
			 			print "district found"
			 			programme = Programme(ingo = obj_ingo, program_name = Program_name, description = Description,
			 			year = Duration,budget = Budget, donors = Donor,district = mydistrict, 
			 			contact_person= Contact_person)
			 			programme.save()
			 			print "programme saved"
			 		except:
			 			#mydistrict="N/A"
			 			mydistrict = District.manager.get(district_name = "Kathmandu")
			 			
			 			print mydistrict.district_name
			 			programme = Programme(ingo = obj_ingo, program_name = Program_name, description = Description,
			 			year = Duration,budget = Budget, donors = Donor,district = mydistrict, 
			 			contact_person= Contact_person)
			 			programme.save()
			 			print "programme saved"
		 	#now proceed for loading programme
		 	except I_ngo.DoesNotExist:
		 		#save ngo list to database
		 		print "New ingo"
			 	district = District.manager.get(district_name= "Kathmandu")
			 	ingo = I_ngo(name=Ingo_name.strip(),address=district,contact_person=Contact_person.strip(),contact_number=Contact_no,org_type= Org_type)
			 	ingo.save()
			 	print "Ingo Saved"
		 		

		 		obj_ingo = I_ngo.manager.get(name= Ingo_name.strip())
		 		Districts = sheet.cell_value(row, DISTRICTS)	
			 	dist_list=[]

			 	for eachdistricts in district_l:
			 		if Districts.find(eachdistricts)!=-1:
			 			dist_list.append(eachdistricts)
			 	print Ingo_name
			 	print Program_name
			 	print dist_list
			 	print Donor
			 	print Budget
			 	print Duration
			 	print Sector
			 	print Contact_person
			 	for eachdist in dist_list:
			 		try:
			 			print "trying to save programme1"
			 			mydistrict = District.manager.get(district_name = eachdist)
			 			print "district found"
			 			programme = Programme(ingo = obj_ingo, program_name = Program_name, description = Description,
			 			year = Duration,budget = Budget, donors = Donor,district = mydistrict, 
			 			contact_person= Contact_person)
			 			programme.save()
			 			print "programme saved"
			 		except:
			 			mydistrict = District.manager.get(district_name = "Kathmandu")
			 			programme = Programme(ingo = obj_ingo, program_name = Program_name, description = Description,
			 			year = Duration,budget = Budget, donors = Donor,district = mydistrict, 
			 			contact_person= Contact_person)
			 			programme.save()
			 			print "programme saved"