# Create your views here.
from iaua_app.models import *
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
import json
from django.views.decorators.csrf import csrf_exempt
import random
import csv


def default_page(request):
	return render_to_response("result.html")
def load_ngos(request):
	rows = []
	with open('ngo.csv', 'rb') as f:
	    reader = csv.reader(f)
	    for row in reader:
	        rows.append(row)
	turn = 1
	lab2csv = csv.writer(open("ngolist.csv","wb"))

	for i in range(len(rows)):
	    	row = rows[i]
	    	try:
		    	if(turn==int(row[0])):
		    		name = row[2]
		    		address = row[7]
		    		contact_person = row[4]
		    		phone_no = row[5]
		    		if(str(turn+1)!=rows[i+1][0]):
		    			row = rows[i+1]
		    			name +=" " + row[2]
			    		address += " " + row[7]
			    		contact_person += " " + row[4]
			    		phone_no += " " + row[5]
			    	if(str(turn+1)!=rows[i+2][0]):
		    			row = rows[i+2]
		    			name += " " + row[2]
			    		address += " " + row[7]
			    		contact_person += " " + row[4]
			    		phone_no += " " + row[5]
			    	if(address.find('Women')!=-1):
			    		address = rows[i][8]
			    	try:
				    	district = District.manager.get(district_name=address.strip())
				    	ingo = I_ngo(name=name.strip(),address=district,contact_person=contact_person.strip(),contact_number=phone_no,org_type="ngo")
				    	ingo.save()
				except:
					pass
			    	myrow = name, address, contact_person, phone_no
		        	lab2csv.writerow(myrow)
		        	turn+=1
		except:
		    	pass

	return HttpResponse("success")
			  
