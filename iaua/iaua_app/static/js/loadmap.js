var dxcord = 84.4; var dycord = 28.48; var mapzoomlevel=7;

var map = L.map('map').setView([dycord, dxcord],mapzoomlevel); //28.48, 84.4], 7);

		L.tileLayer('http://{s}.tile.cloudmade.com/{key}/22677/256/{z}/{x}/{y}.png', {
			attribution: 'I/NGO AID Utility Analyzer, 2013',
			key: 'BC9A493B41014CAABB98F0471D759707'
		}).addTo(map);

		var baseballIcon = L.icon({
			iconUrl: 'baseball-marker.png',
			iconSize: [32, 37],
			iconAnchor: [16, 37],
			popupAnchor: [0, -28]
		});
var dist = {};

var lastClickedDistrict = null;


	districtcenter={	
'HUMLA':{'lat':29.979272824940068,'lon':81.8222916109113}
,'DARCHULA':{'lat':29.88140322935043,'lon':80.75076277465905}
,'BAJHANG':{'lat':29.761885913669083,'lon':81.15118345233806}
,'MUGU':{'lat':29.642410619499557,'lon':82.34985171699738}
,'BAJURA':{'lat':29.593646264073712,'lon':81.54060891709322}
,'BAITADI':{'lat':29.490174024456554,'lon':80.55176451086962}
,'DOLPA':{'lat':29.205452267710644,'lon':83.07687629530184}
,'JUMLA':{'lat':29.264886352272715,'lon':82.21834409090899}
,'KALIKOT':{'lat':29.206874123577215,'lon':81.77063355772363}
,'DOTI':{'lat':29.12172976579138,'lon':80.90053759829662}
,'DADELDHURA':{'lat':29.21874021262766,'lon':80.45946471680593}
,'ACHHAM':{'lat':29.079078778366107,'lon':81.28673875416028}
,'MUSTANG':{'lat':28.993616669625254,'lon':83.85784697140052}
,'DAILEKH':{'lat':28.897324035200004,'lon':81.63099449919987}
,'JAJARKOT':{'lat':28.870002223988447,'lon':82.18628259537572}
,'KANCHANPUR':{'lat':28.834148168130486,'lon':80.31957270890834}
,'KAILALI':{'lat':28.83170552083338,'lon':80.87180366666657}
,'RUKUM':{'lat':28.680595918194605,'lon':82.68671192806765}
,'SURKHET':{'lat':28.69218120308484,'lon':81.51886776692378}
,'MANANG':{'lat':28.68649810220129,'lon':84.21650905188689}
,'MYAGDI':{'lat':28.54330703110598,'lon':83.47442132949307}
,'GORKHA':{'lat':28.292833937037,'lon':84.76423427685194}
,'BARDIYA':{'lat':28.346707930501953,'lon':81.41049750064344}
,'SALYAN':{'lat':28.411038754901984,'lon':82.09607447712419}
,'BAGLUNG':{'lat':28.35339136240311,'lon':83.25013466279077}
,'KASKI':{'lat':28.323715631840802,'lon':84.00117718905469}
,'ROLPA':{'lat':28.327428439429934,'lon':82.62461928741095}
,'LAMJUNG':{'lat':28.258047461100574,'lon':84.42238218026577}
,'PARBAT':{'lat':28.211983027079295,'lon':83.67880127079303}
,'RASUWA':{'lat':28.211737664201173,'lon':85.41851405325444}
,'PYUTHAN':{'lat':28.124220146608334,'lon':82.8882722122538}
,'DHADING':{'lat':27.933017250588225,'lon':84.97569103647062}
,'BANKE':{'lat':28.083415577946774,'lon':81.79876676235745}
,'GULMI':{'lat':28.09904484534269,'lon':83.35101080316342}
,'DANG':{'lat':27.986445170854253,'lon':82.45116397989943}
,'SYANGJA':{'lat':28.001706233524352,'lon':83.7530468882521}
,'SINDHUPALCHOK':{'lat':27.886487447058816,'lon':85.76853125441177}
,'DOLAKHA':{'lat':27.785949831516373,'lon':86.19190194945486}
,'TANAHU':{'lat':27.939959324396845,'lon':84.24810663002683}
,'ARGHAKHANCHI':{'lat':27.910357628205126,'lon':83.05109165897436}
,'SOLUKHUMBU':{'lat':27.77586600303642,'lon':86.72765746659925}
,'NUWAKOT':{'lat':27.921010840864447,'lon':85.22501892927313}
,'SANKHUWASABHA':{'lat':27.59085652582499,'lon':87.29540935796274}
,'PALPA':{'lat':27.85153857509155,'lon':83.65728530586075}
,'TAPLEJUNG':{'lat':27.599797264367812,'lon':87.82840435318701}
,'CHITWAN':{'lat':27.573632563829772,'lon':84.4017084773937}
,'NAWALPARASI':{'lat':27.681676585321096,'lon':84.03389125137616}
,'KAPILBASTU':{'lat':27.632803676240215,'lon':82.94848615665799}
,'RAMECHHAP':{'lat':27.544529829714286,'lon':86.20491396571428}
,'KATHMANDU':{'lat':27.704517404692055,'lon':85.3391681348974}
,'RUPANDEHI':{'lat':27.571393573770482,'lon':83.39121525901648}
,'KAVREPALANCHOK':{'lat':27.531323010238896,'lon':85.62183809385671}
,'BHAKTAPUR':{'lat':27.681279624203807,'lon':85.43816261783435}
,'MAKWANPUR':{'lat':27.44508893732587,'lon':85.15614445961}
,'LALITPUR':{'lat':27.552832115131572,'lon':85.34888545065796}
,'OKHALDHUNGA':{'lat':27.315615147727264,'lon':86.40455311590907}
,'BHOJPUR':{'lat':27.182023405342633,'lon':87.07219884785121}
,'PARSA':{'lat':27.23856834354485,'lon':84.76411532603937}
,'SINDHULI':{'lat':27.21245780805687,'lon':85.87698956635062}
,'KHOTANG':{'lat':27.175921158209015,'lon':86.79563906417904}
,'PANCHTHAR':{'lat':27.155394185131193,'lon':87.80840249416914}
,'BARA':{'lat':27.107717334894637,'lon':85.05671584309133}
,'TERHATHUM':{'lat':27.14247141298703,'lon':87.53226616883119}
,'RAUTAHAT':{'lat':26.999850569518706,'lon':85.32896578877005}
,'DHANKUTA':{'lat':26.991858955149493,'lon':87.34217434053147}
,'SARLAHI':{'lat':26.960720882105267,'lon':85.53664856842114}
,'UDAYAPUR':{'lat':26.91613755869873,'lon':86.7346036209335}
,'MAHOTTARI':{'lat':26.86530347103274,'lon':85.83208717128468}
,'ILAM':{'lat':26.865068647297292,'lon':87.93552327837838}
,'DHANUSHA':{'lat':26.7960594044944,'lon':86.01894429213479}
,'SIRAHA':{'lat':26.744034551246518,'lon':86.32254278947373}
,'SAPTARI':{'lat':26.621490283367528,'lon':86.7465458870636}
,'MORANG':{'lat':26.603627821276593,'lon':87.46163149645383}
,'SUNSARI':{'lat':26.638756034129685,'lon':87.17126156484649}
,'JHAPA':{'lat':26.585266281400955,'lon':87.93457473913045}
};

var sVars = ['Taplejung','Panchthar','Ilam','Jhapa','Morang','Sunsari','Dhankuta','Terhathum','Sankhuwasabha','Bhojpur','Solukhumbu','Okhaldhunga','Khotang','Udayapur','Saptari','Siraha','Dhanusha','Mahottari','Sarlahi','Sindhuli','Ramechhap','Dolakha','Sindhupalchok','Kavrepalanchok','Lalitpur','Bhaktapur','Kathmandu','Nuwakot','Rasuwa','Dhading','Makwanpur','Rautahat','Bara','Parsa','Chitwan','Gorkha','Lamjung','Tanahu','Syangja','Kaski','Manang','Mustang','Myagdi','Parbat','Baglung','Gulmi','Palpa','Nawalparasi','Rupandehi','Kapilbastu','Arghakhanchi','Pyuthan','Rolpa','Rukum','Salyan','Dang','Banke','Bardiya','Surkhet','Dailekh','Jajarkot','Dolpa','Jumla','Kalikot','Mugu','Humla','Bajura','Bajhang','Achham','Doti','Kailali','Kanchanpur','Dadeldhura','Baitadi','Darchula'];
function genetatebubbles()
{
	for(i=0;i<sVars.length;i++)
	{
		var feature = districtcenter[sVars[i].toUpperCase()];
	var lon = feature.lon;
	var districtname = sVars[i].toLowerCase();
	var ingono = ingodata[districtname.charAt(0).toUpperCase() + districtname.slice(1)];
	var radius = 9000*(ingono/653);//Math.pow(5.34,(ingono/30));
	//console.log(ingono+districtname);
			var lat = feature.lat;
			L.circle([lat, lon], radius, {
			color: 'blue',
			fillColor: 'blue',
			fillOpacity: 1
		}).addTo(map).bindPopup("No. of NGOs in "+districtname+" : "+ingono);
	}
}


var currentdistrict = "Kathmandu";


function showngolist(dis_name)
{

	currentdistrict = dis_name;
	$.ajax({ 
           

            type:"post",
            dataType:"json",
            url: "ngolist",
            data: "district="+dis_name,
            success: function(data){
                retVal = data.result;
                content = '<table class="table"><thead><tr><th>#</th><th>NGO Name</th><th>Contact Person</th>';
                content += '<th>contact No.</th></tr></thead><tbody>';
                row = "";
                for(i=0;i<retVal.length;i++)
                {
                row += "<tr><td>"+(i+1)+"</td><td>"+retVal[i].name+"</td><td>"+retVal[i].contact_person+"</td><td>"+retVal[i].contact_number+"</td></tr>";
                }
                content += row;
                content += '</tbody></table>';
                $("#modelcontent").html(content);
                $("#myModalLabel").html(dis_name);

	document.getElementById("showmodel").click();
        }

                    
            

           });

}

function gotodistrict(e) {
	
			

var layer = e.target;
			if (!L.Browser.ie && !L.Browser.opera) {
				//layer.bringToFront();
			}
			var districtname = (layer.feature.properties.Name).toLowerCase();

			districtname = districtname.charAt(0).toUpperCase() + districtname.slice(1);
			
			showngolist(districtname);
		}



		var info = L.control();


		info.onAdd = function (map) {
			this._div = L.DomUtil.create('div', 'info');
			this.update();
			return this._div;
		};

		info.update = function (props) {
			this._div.innerHTML = '<h4>District</h4>' +  (props ?
				'<b>' + props.Name : 'Click on a district to find Schools');
		};

		info.addTo(map);

function highlightFeature(e) {
			var layer = e.target;
			if (!L.Browser.ie && !L.Browser.opera) {
				//layer.bringToFront();
			}

			info.update(layer.feature.properties);
		}



		



		function onEachFeature(feature, layer) {
			

			var popupContent = "<p>";
			var i=1;
			
			popupContent +="</p>";

			layer.on({

				mouseover: highlightFeature,
				click: gotodistrict
			});


		}
	
		
function style(feature) {
	
    return {
        fillColor: 'white',
        weight: 1,
        opacity: 1,
        color: 'blue',
        dashArray: '3',
        fillOpacity: 0.7
    };
}

geojson = L.geoJson(districtmap, {
			style: style,
			onEachFeature: onEachFeature
		}).addTo(map);
genetatebubbles();



